/*global module:false*/
module.exports = function(grunt) {
    "use strict";
    grunt.initConfig({
        //run grunt utilites AND the app
        concurrent: {
            dev: {
                tasks: ['nodemon', 'watch'],
            },
            options: {
                logConcurrentOutput: true
            }
        },
        jshint: {
            files: ['Gruntfile.js', 'src/**/*.js', 'tests/**/*.js']
        },
        nodemon: {
            dev: {
                script: 'src/app.js',
            }
        },
        watch: {
           dev : {
               files: ['src/**/*.js', 'tests/**/*.js'],
               tasks: ['test', 'jsdoc']
           }
        },
        nodeunit: {
            test: {
                src: ['tests/**/*.js']
            }
        },
        jsdoc : {
            dist: {
                src: ['src/**/*.js', 'tests/**/*.js'],
                options: {
                    destination: 'doc'
                }
            }
        }
    });
    grunt.loadNpmTasks('grunt-contrib-nodeunit');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-concurrent');
    grunt.loadNpmTasks('grunt-nodemon');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-jsdoc');
    grunt.registerTask('test', ['jshint', 'nodeunit']);
    grunt.registerTask('default', ['test', 'jsdoc', 'concurrent']);
};
