/*jslint node: true*/
"use strict";
/**
* unit tests for the producer
* @module tests/producer 
*/
var Producer = require('../src/producer.js');
var producer = new Producer("TestProducer");
exports.test = {
    'expects name to be set': function(test) {
       test.expect(1);
       test.equals(producer.name, "TestProducer");
        test.done();
    },
    'expects reasonable defaults': function(test) {
        var options = {
            host: "localhost",
            port: 9999,
            path: '',
            method: 'GET',
        };
        test.expect(1);
        test.deepEqual(producer.options, options);
        test.done();
    },
    'expects producer to call a test api': function(test) {
        test.expect(1);
        producer.options.host = "jsonplaceholder.typicode.com";
        producer.options.port = 80;
        producer.request("/posts")(function(response) {
            var data = JSON.parse(response);
            test.equals(data.length, 100);
            test.done();
        });
    }
};
