/*jslint node:true */
"use strict";
/**
* unit tests for the calculator
* @module tests/calculator
*/
var calculator = require('../src/calculator.js');
exports.test = {
    'expects 1 + 1 = 2': function(test) {
        var params = { val: [1,1] };
        test.expect(1);
        calculator.add(params)(function(result) {
            test.equal(result, 2, "Addition works");
            test.done();
        });
    },
    'expects 1 + 1 + 1 + 1 = 4': function(test) {
        var params = { val: [1,1,1,1] } ;
        test.expect(1);
        calculator.add(params)(function(result) {
            test.equal(result, 4);
            test.done();
        });
    },
    "expects 1 + '1' + 1 = 3": function(test) {
        var params = { val: [1, '1', 1] };
        test.expect(1);
        calculator.add(params)(function(result) {
            test.equal(result, 3);
            test.done();
        });
    },
    "expects divided by 0 to fail": function(test) {
        var params = {val: [100, 0] };
        test.expect(1);
        calculator.divide(params)(
            function(result) {},
            function(error) {
                test.equal(error, "Can't divide by zero");
                test.done();
            }
        );
    },
    "expects 4 / '2' = 2": function(test) {
        var params = {val: [4, '2'] };
        test.expect(1);
        calculator.divide(params)(function(result) {
            test.equal(result, 2);
            test.done();
        });
    },
    "expects 4 * '4' = 16": function(test) {
        var params = {val: [4, '4'] };
        test.expect(1);
        calculator.multiply(params)(function(result) {
            test.equal(result, 16);
            test.done();
        });
    }
};
