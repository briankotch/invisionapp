/*jslint node: true*/
"use strict";
/**
* unit tests for the consumer
* @module tests/consumer
*/
var consumer = require('../src/consumer.js');
exports.test = {
    'expects validator to succeed': function(test) {
        test.expect(1);
        var params = { val: [1, 1, 1]};
        consumer.validate('GET', 'add', params)(function(valid){
            test.equal(valid, true);
            test.done();
        }); 
        
        
    },
    'expects route to do its job': function(test) {
        //minimal mock simulating a request
        var request = {
            url: "/add?val=1&val=1&val=1",
            method: "GET"
        };
        var response = {};
        test.expect(2);
        consumer.route(request, response)(function(response, httpCode, result){
            test.equal(httpCode, 200); 
            test.equal(result, 3);
            test.done();
        });
    },
};
