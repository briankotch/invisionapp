Asynchronous calculator api written in Node
====

Tools:
----
* Strict, linted Javascript
* Automated workflow in grunt
* Unit tests in nodeunit
* No frameworks  (I do use winston for logging, because async streaming logs is not something anyone should every jhave to write).

Running it
---
        
        npm install
        grunt

Testing it
---
        grunt test

Updating documentation
---
        grunt jsdoc


        
This will lint, test and then run the app in node mode. The app also spawns two producers that hammer the api.

After it finishes the hammer, it will continue to listen on 9999 for GET calls.

Logs are generated in the logs/ directory

