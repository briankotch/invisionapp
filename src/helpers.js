/*jslint node:true */
"use strict";
/**
* various helper functions 
* @module helpers
*/

/**
* removes forward and backslashes
* @param {string} text the text to be stripped.
* @returns {string} text san slashes
*/
exports.stripSlashes = function(text) {
    return text.replace(/\//g, '');
};

/**
* generates a random number, including the low and high bounds
* @param {integer} low the lower limit of the possible ints
* @param {integer} high the upper limit of the possible ints
* @returns {integer} a number in the range, inclusive.
*/
exports.randomIntInc = function (low, high) {
    return Math.floor(Math.random() * (high - low + 1) + low);
};

/**
* generates an array of a certain size with randoms numbers including the low and high bounds
* @param {integer} size the size of the array to generate
* @param {integer} low the lower limit of the possible ints
* @param {integer} high the upper limit of the possible ints
* @returns {array} a number in the range, inclusive.
*/
exports.randomIntArray = function(size, low, high) {
    var numbers = new Array(size);
    for (var i = 0; i < numbers.length; i++) {
        numbers[i] = exports.randomIntInc(low,high);
    }
    return numbers;
};
