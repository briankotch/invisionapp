/*jslint node:true */
"use strict";

/**
* Spins up the consumer service and has it listen for http requests on port 9999
* Spins up the producer service andgenerate requests
* @module app
*/
var consumer = require('./consumer.js');
var helper = require('./helpers.js');
var winston = require("winston");
var Producer = require('./producer.js');
var producer1 = new Producer("Producer 1");
var producer2 = new Producer("Producer 2");

var logger = new(winston.Logger)({
    transports: [
        new(winston.transports.Console)(),
        new(winston.transports.File)({
            filename: process.cwd() + '/logs/app.log'
        })
    ]
});

logger.log("info", "Starting http server");
consumer.createServer(9999, function() {
    producer1.dispatch(0,100);
    producer2.dispatch(0,100);
});
