## Modules
<dl>
<dt><a href="#module_app">app</a></dt>
<dd><p>Spins up the consumer service and has it listen for http requests on port 9999
Spins up the producer service andgenerate requests</p>
</dd>
<dt><a href="#module_calculator">calculator</a></dt>
<dd><p>math!</p>
</dd>
<dt><a href="#module_consumer">consumer</a></dt>
<dd><p>webservice for processing calculator requests</p>
</dd>
<dt><a href="#module_helpers">helpers</a></dt>
<dd><p>various helper functions</p>
</dd>
</dl>
## Classes
<dl>
<dt><a href="#producer">producer</a></dt>
<dd></dd>
</dl>
<a name="module_app"></a>
## app
Spins up the consumer service and has it listen for http requests on port 9999
Spins up the producer service andgenerate requests

<a name="module_calculator"></a>
## calculator
math!


* [calculator](#module_calculator)
  * [.add(params, callback, errorCallback)](#module_calculator.add) ⇒ <code>int</code>
  * [.subtract(params, callback, errorCallback)](#module_calculator.subtract) ⇒ <code>int</code>
  * [.divide(params, callback, errorCallback)](#module_calculator.divide) ⇒ <code>int</code>
  * [.multiply(params, callback, errorCallback)](#module_calculator.multiply) ⇒ <code>int</code>

<a name="module_calculator.add"></a>
### calculator.add(params, callback, errorCallback) ⇒ <code>int</code>
adds values together

**Kind**: static method of <code>[calculator](#module_calculator)</code>  
**Returns**: <code>int</code> - sum  

| Param | Type | Description |
| --- | --- | --- |
| params | <code>object</code> | object containing parameters to calculate |
| params.val | <code>array</code> | array of numbers to calculate |
| callback | <code>function</code> | function to return the value to |
| errorCallback | <code>function</code> | function to return errors to |

<a name="module_calculator.subtract"></a>
### calculator.subtract(params, callback, errorCallback) ⇒ <code>int</code>
subtract values

**Kind**: static method of <code>[calculator](#module_calculator)</code>  
**Returns**: <code>int</code> - difference  

| Param | Type | Description |
| --- | --- | --- |
| params | <code>object</code> | object containing parameters to calculate |
| params.val | <code>array</code> | array of numbers to calculate |
| callback | <code>function</code> | function to return the value to |
| errorCallback | <code>function</code> | function to return errors to |

<a name="module_calculator.divide"></a>
### calculator.divide(params, callback, errorCallback) ⇒ <code>int</code>
divide values, will error on dividing by 0

**Kind**: static method of <code>[calculator](#module_calculator)</code>  
**Returns**: <code>int</code> - quotient  

| Param | Type | Description |
| --- | --- | --- |
| params | <code>object</code> | object containing parameters to calculate |
| params.val | <code>array</code> | array of numbers to calculate |
| callback | <code>function</code> | function to return the value to |
| errorCallback | <code>function</code> | function to return errors to |

<a name="module_calculator.multiply"></a>
### calculator.multiply(params, callback, errorCallback) ⇒ <code>int</code>
multiply values

**Kind**: static method of <code>[calculator](#module_calculator)</code>  
**Returns**: <code>int</code> - product  

| Param | Type | Description |
| --- | --- | --- |
| params | <code>object</code> | object containing parameters to calculate |
| params.val | <code>array</code> | array of numbers to calculate |
| callback | <code>function</code> | function to return the value to |
| errorCallback | <code>function</code> | function to return errors to |

<a name="module_consumer"></a>
## consumer
webservice for processing calculator requests


* [consumer](#module_consumer)
  * _static_
    * [.validate(requestMethod, requestUrl, params, validCallback, errorCallback)](#module_consumer.validate)
    * [.route(http, http, function)](#module_consumer.route)
    * [.createServer(port)](#module_consumer.createServer)
  * _inner_
    * [~validCallback](#module_consumer..validCallback) : <code>function</code>
    * [~response](#module_consumer..response) : <code>function</code>

<a name="module_consumer.validate"></a>
### consumer.validate(requestMethod, requestUrl, params, validCallback, errorCallback)
validate an extracted response
only gets to calculator functions with an array of val parameters are permitted

**Kind**: static method of <code>[consumer](#module_consumer)</code>  

| Param | Type | Description |
| --- | --- | --- |
| requestMethod | <code>string</code> | the HTTP method we're calling (GET) |
| requestUrl | <code>string</code> | the url we are calling |
| params | <code>object</code> | parameters passed into the request |
| params.val | <code>array</code> | list of numbers to do math on |
| validCallback | <code>validCallback</code> | function to call when we pass |
| errorCallback | <code>respond</code> | function to call when we fail |

<a name="module_consumer.route"></a>
### consumer.route(http, http, function)
route a http request and update its response

**Kind**: static method of <code>[consumer](#module_consumer)</code>  

| Param | Type | Description |
| --- | --- | --- |
| http | <code>request</code> | request object |
| http | <code>request</code> | response object |
| function | <code>respond</code> | to return the response |

<a name="module_consumer.createServer"></a>
### consumer.createServer(port)
createServer

**Kind**: static method of <code>[consumer](#module_consumer)</code>  

| Param | Type | Description |
| --- | --- | --- |
| port | <code>integer</code> | port to listen on |

<a name="module_consumer..validCallback"></a>
### consumer~validCallback : <code>function</code>
**Kind**: inner typedef of <code>[consumer](#module_consumer)</code>  

| Param | Type | Description |
| --- | --- | --- |
| valid | <code>boolean</code> | whether or not the validation was true |

<a name="module_consumer..response"></a>
### consumer~response : <code>function</code>
complete the request by responding to the client

**Kind**: inner typedef of <code>[consumer](#module_consumer)</code>  

| Param | Type | Description |
| --- | --- | --- |
| response | <code>object</code> | http response object to send back |
| httpCode | <code>int</code> | http code for the processed request |
| message | <code>string</code> | the response |

<a name="module_helpers"></a>
## helpers
various helper functions


* [helpers](#module_helpers)
  * [.stripSlashes(text)](#module_helpers.stripSlashes) ⇒ <code>string</code>
  * [.randomIntInc(low, high)](#module_helpers.randomIntInc) ⇒ <code>integer</code>
  * [.randomIntArray(size, low, high)](#module_helpers.randomIntArray) ⇒ <code>array</code>

<a name="module_helpers.stripSlashes"></a>
### helpers.stripSlashes(text) ⇒ <code>string</code>
removes forward and backslashes

**Kind**: static method of <code>[helpers](#module_helpers)</code>  
**Returns**: <code>string</code> - text san slashes  

| Param | Type | Description |
| --- | --- | --- |
| text | <code>string</code> | the text to be stripped. |

<a name="module_helpers.randomIntInc"></a>
### helpers.randomIntInc(low, high) ⇒ <code>integer</code>
generates a random number, including the low and high bounds

**Kind**: static method of <code>[helpers](#module_helpers)</code>  
**Returns**: <code>integer</code> - a number in the range, inclusive.  

| Param | Type | Description |
| --- | --- | --- |
| low | <code>integer</code> | the lower limit of the possible ints |
| high | <code>integer</code> | the upper limit of the possible ints |

<a name="module_helpers.randomIntArray"></a>
### helpers.randomIntArray(size, low, high) ⇒ <code>array</code>
generates an array of a certain size with randoms numbers including the low and high bounds

**Kind**: static method of <code>[helpers](#module_helpers)</code>  
**Returns**: <code>array</code> - a number in the range, inclusive.  

| Param | Type | Description |
| --- | --- | --- |
| size | <code>integer</code> | the size of the array to generate |
| low | <code>integer</code> | the lower limit of the possible ints |
| high | <code>integer</code> | the upper limit of the possible ints |

<a name="producer"></a>
## producer
**Kind**: global class  

* [producer](#producer)
  * [new producer(name)](#new_producer_new)
  * [.self.options](#producer.self.options)
  * [.self.dispatch(dispatched, goal)](#producer.self.dispatch)
  * [.self.request(action, url, the)](#producer.self.request)

<a name="new_producer_new"></a>
### new producer(name)
http request dispatcher class


| Param | Type | Description |
| --- | --- | --- |
| name | <code>string</code> | the name of the producer (for outputs) |

<a name="producer.self.options"></a>
### producer.self.options
http request configuration

**Kind**: static property of <code>[producer](#producer)</code>  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| options | <code>object</code> | http request object |
| options.host | <code>string</code> | url |
| options.port | <code>integer</code> | port number |
| options.path | <code>string</code> | path to call |
| options.method | <code>string</code> | http method, only gets are supported right now |

<a name="producer.self.dispatch"></a>
### producer.self.dispatch(dispatched, goal)
recursive function for hammering the request object with random parameters

**Kind**: static method of <code>[producer](#producer)</code>  

| Param | Type | Description |
| --- | --- | --- |
| dispatched | <code>integer</code> | the number of calls we've made. |
| goal | <code>integer</code> | the number of calls we want to get to |

<a name="producer.self.request"></a>
### producer.self.request(action, url, the)
make an http request, bare minimum options

**Kind**: static method of <code>[producer](#producer)</code>  

| Param | Type | Description |
| --- | --- | --- |
| action | <code>string</code> | the path to the end point |
| url | <code>object</code> | params to be built into the query string |
| the | <code>function</code> | callback to fire when all data is received |

