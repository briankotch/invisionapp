/*jslint node:true */
"use strict";
/**
* math!
* @module calculator
*/

/**
* adds values together
* @param params {object} object containing parameters to calculate
* @param params.val {array} array of numbers to calculate
* @param callback {function} function to return the value to
* @param errorCallback {function} function to return errors to
* @return {int} sum
*/
exports.add = function(params) {
    return function(callback, errorCallback) {
        var sum = 0;
        for(var i = 0; i < params.val.length; i++) {
            sum = sum + parseInt(params.val[i], 10);
        }
        callback(sum);
    };
};

/**
* subtract values
* @param params {object} object containing parameters to calculate
* @param params.val {array} array of numbers to calculate
* @param callback {function} function to return the value to
* @param errorCallback {function} function to return errors to
* @return {int} difference
*/
exports.subtract = function(params) {
    return function(callback) {
        var difference = null;
        for(var i = 0; i < params.val.length; i++) {
            if(difference === null) {
               difference = parseInt(params.val[i], 10);
            } else {
               difference = difference - parseInt(params.val[i], 10);
            }
        }
        callback(difference);
    };
};

/**
* divide values, will error on dividing by 0
* @param params {object} object containing parameters to calculate
* @param params.val {array} array of numbers to calculate
* @param callback {function} function to return the value to
* @param errorCallback {function} function to return errors to
* @return {int} quotient
*/
exports.divide = function(params) {
    return function(callback, errorCallback) {
        var quotient = null;
        for(var i = 0; i < params.val.length; i++) {
            var value = parseInt(params.val[i], 10);
            if(value === 0) {
                errorCallback("Can't divide by zero");
            }
            if(quotient === null) {
                quotient = value;
            } else {
                quotient = quotient / value;
            }
        }
        callback(quotient);
    };
};

/**
* multiply values
* @param params {object} object containing parameters to calculate
* @param params.val {array} array of numbers to calculate
* @param callback {function} function to return the value to
* @param errorCallback {function} function to return errors to
* @return {int} product
*/
exports.multiply = function(params) {
    return function(callback, errorCallback) {
        var product = 1;
        for(var i = 0; i < params.val.length; i++) {
            product = product * parseInt(params.val[i]);
        }
        callback(product);
    };
};
