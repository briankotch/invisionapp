/*jslint node:true*/
"use strict";
var http = require("http");
var helpers = require("./helpers.js");
var querystring = require("querystring");
var winston = require("winston");
var logger = new(winston.Logger)({
    transports: [
        new(winston.transports.Console)(),
        new(winston.transports.File)({
            filename: process.cwd() + '/logs/producer.log'
        })
    ]
});
/**
* http request dispatcher class
* @class producer
* @param {string} name the name of the producer (for outputs)
*/
function Producer(name) {
    var self = this;
    var operations = ['/add', '/subtract', '/multiply', '/divide'];
    self.name = name;

    /**
     * http request configuration
     * @memberof producer
     * @property {object} options http request object
     * @property {string} options.host url
     * @property {integer} options.port port number
     * @property {string} options.path path to call
     * @property {string} options.method http method, only gets are supported right now
    */
    self.options = {
        host: "localhost",
        port: 9999,
        path: '',
        method: 'GET',
    };
    
    /**
     * recursive function for hammering the request object with random parameters
     * @memberof producer
     * @param {integer} dispatched the number of calls we've made.
     * @param {integer} goal the number of calls we want to get to
     */
    self.dispatch = function(dispatched, goal) {
        var params = { val: helpers.randomIntArray(10, -10, 10) };
        var action = operations[helpers.randomIntInc(0, 3)];
        logger.log("info", self.name + " dispatched: " + dispatched);
        self.request(action, params)(function(response) {
            logger.log("info", self.name + " received " + response);
            if(dispatched < goal) {
                dispatched = dispatched + 1;
                self.dispatch(dispatched, goal);
            }  
        });
    };
    
    /**
     * make an http request, bare minimum options
     * @memberof producer
     * @param {string} action the path to the end point
     * @param {object} url params to be built into the query string
     * @param {function} the callback to fire when all data is received
     */
    self.request = function(action, params) {
        return function(callback) {
            //build and make the request
            logger.log("info", "Requesting " + action);
            var path = action;
            if(params !== 'undefined') {
                path = action + "?" + querystring.stringify(params);
            }
            self.options.path = path;
            http.request(self.options, function(res) {
                var data = "";
                res.on('data', function(chunk) {
                    data += chunk;
                });
                res.on('end', function() {
                    //callback on data complete
                    callback(data);
                });
            }).end();
        };
    };

}
module.exports = Producer;

